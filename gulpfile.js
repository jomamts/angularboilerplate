var  gulp = require('gulp');
var connect = require('gulp-connect');

gulp.task('connect', function () {
    connect.server({
        root: 'app',
        port: 37374,
        livereload: true
    });
});